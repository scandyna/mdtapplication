
# [0.5.3] - 2024-12-XX

## Fixed

- Conan packages: define Qt components in `package_info()` (https://gitlab.com/scandyna/mdtapplication/-/issues/4)
- Use `mdtcmakemodules` v0.20.0 (https://gitlab.com/scandyna/mdtapplication/-/issues/7)


# [0.5.2] - 2024-11-11

## Changed

- Updates in Conan packages (https://gitlab.com/scandyna/mdtapplication/-/issues/7)

# [0.5.1] - 2024-11-09

## Changed

- Make Conan package names lowercase (https://gitlab.com/scandyna/mdtapplication/-/issues/7)


# [0.4.5] - 2023-01-09

## Fixed

- https://gitlab.com/scandyna/mdtapplication/-/issues/4


# [0.4.4] - 2022-10-13

## Changed

- Use MdtCMakeModules 0.19.1


# [0.4.3] - 2022-09-20

## Changed

- Use Qt 5.15.6

# [0.4.1] - 2022-08-26

## Changed

- Switching to C++17

# [0.4.0] - 2022-08-25

## Changed

- Split Conan packages
- Conan packages: migrate to CMakeDeps / CMakeToolchain
- Internal: restructure project

## Fixed

- https://gitlab.com/scandyna/mdtapplication/-/issues/1
